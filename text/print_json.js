/*
 *  標準入力からyanashe語録のjsonを受け取って
 *  textだけ出力する.
 */

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function(chunk){
  var json = JSON.parse(chunk);
  for( var i = 0; i < json.length; i++){
    console.log(json[i].text);
  }
});

process.stdin.on('end', function(){
  // 終了時に呼ばれる. 特に何もしない.
});
