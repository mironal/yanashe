var util = require('util');

process.stdin.resume();
process.stdin.setEncoding('utf8');

var inputText = "";
process.stdin.on('data', function(chunk){
  /* 入力が実行しながら出力するコマンド(例えばmecabとか)のパイプだと、
   * 予想外の位置で分割されたchunkが入ってくる為
   * dataイベントでは全ての入力を接続するだけにする.
   * endイベントで入力に対する処理を行う.
   *
   * なお、入力がcat file　とかのパイプだと一つのchunkで入ってくる
   *
  /*
  console.log("data");
  console.log(chunk);
  */
  inputText += chunk;
});

process.stdin.on('end', function(){
  var result = new Array();
  var obj = new Object();
  obj.text = "";
  obj.words = new Array();


  inputText.split('\n').forEach(function(line){

    if( line === "EOS" ){
      result.push(obj);

      // オブジェクト再構築
      obj = new Object();
      obj.text = "";
      obj.words = new Array();
    }else if( line.length > 0){
      var token = line.split('\t');
      var word = token[0];
      var info = token[1];

      obj.text += word;
      var wordObj = new Object();
      wordObj.word = word;
      wordObj.info = info.split(',');
      obj.words.push(wordObj);
    }
  });

  /* json欲しいだけならこいつをそのまま出力 */
  var jsonStr = JSON.stringify(result);

  /* 整形するためにオブジェクトにする. */
  var json = JSON.parse(jsonStr);


  var outputStr = "";
  for( var i = 0; i < json.length; i++){
    outputStr += util.inspect(json[i], false, 4);
    outputStr += '\n';
  }

  console.log(outputStr);

});

/*
 * [{"text":"元の文章", "words":[{"word":"表層形","info":["品詞","品詞細分類1",...]},
 *                              {"word":"表層形","info":["品詞","品詞細分類1",...]}, ...]}]
 */
