// cronで実行する際にはcronユーザの環境変数が
// 使用されるため、LANGの設定をちゃんとしないと
// 文字化けする.
console.log("export LANG=ja_JP.UTF-8")
//ディレクトリ移動
console.log("cd `dirname $0`");
//タイムスタンプ
console.log("# " + new Date());

create_jobs_per_day();

function create_jobs_per_day(){
  /*
   * その日のテンション  0～1の間の値をとり、
   * 大きいほどその日のつぶやきの頻度が多くなる確率が高くなる 
   */
  var tension = Math.random();
  console.log('# tension = ' + tension);

  var LIMIT_SEC = 86400; /* 60 * 60 * 24 */
  var sum = 0;
  var sleeps = create_sleep_per_day(tension);

  for( var i = 0; i < sleeps.length; i++){
    console.log("sleep " + sleeps[i]);
    console.log("sh tweet_line.sh");
    sum += sleeps[i];
  }
  if( sum > LIMIT_SEC ){
    /*
     * バグ検出用
     */
    console.log("# sum = " + sum);
  }
}

function create_sleep_per_day(tension){
  /* １日につぶやける回数の限界 */
  var API_LIMIT = 1000;
  var sleeps_per_day = new Array();
  // 1日分のつぶやきを計画する
  for(var i = 0; (i < 24) && (sleeps_per_day.length < API_LIMIT) ; i++){
    sleeps_per_day = sleeps_per_day.concat(
                       create_sleep_per_hour(function(){
                         if((API_LIMIT -  sleeps_per_day.length ) >= API_LIMIT){
                           return API_LIMIT / 10;
                         }else{
                           return API_LIMIT - sleeps_per_day.length;
                         }
                        }() * tension
                       )
                     );
  }
  return sleeps_per_day;
}

/*
 * １時間分のスリープの配列を生成する
 * max_times : この１時間の間に後何回つぶやけるか(スリープ出来るか)
 */
function create_sleep_per_hour(max_times){
  /*
   * １時間あたりのAPIリミット
   *  150回ぐらいは大丈夫そうだけど100ぐらいで...
   */
  var API_LIMIT_HOUR = 100;

  // 大体１時間に何回つぶやくかの計画
  var times = Math.floor(Math.random() * max_times);
  if (times > API_LIMIT_HOUR){
    times = API_LIMIT_HOUR;
  }
  // つぶやき頻度のパラメータ
  var phase = Math.random() * 10;
  var freq  = Math.random() * 10;
  console.log('# times = ' + times);
  console.log('# phase = ' + phase);
  console.log('# freq  = ' + freq);

  var sleeps = new Array();
  var sleep_sum = 0;

  /**
   *  つぶやき頻度が周期的に変わるようにする
   */
  for(var i = 0; i < times; i++){

    // 次のつぶやきまでの大体の待ち時間
    var wait_sec =  Math.abs(Math.cos(i / freq + phase)) * 3600;

    /* 少しノイズを含ませる */
    var noise = Math.random() * (wait_sec / 2);
    wait_sec  = Math.floor(wait_sec + noise);

    // 1時間の間に収まらければ、つぶやかずに
    // 終了する.
    if( (sleep_sum + wait_sec) > 3600 ){
      break;
    }

    // 1時間の間に収まるのであれば、つぶやく.
    sleep_sum += wait_sec;

    // 待ち時間が0の場合はつぶやかない
    // (負荷がかかりそうなため)
    if( wait_sec > 0 ){
      sleeps.push(wait_sec);
    }
  }
  return sleeps;
}

