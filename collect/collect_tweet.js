var http = require('http');

function getUserTimeline(page){
  var baseUrl =  '/1/statuses/user_timeline/106355179.json?exclude_replies=true&trim_user=true';

  var options = {
    host: 'api.twitter.com',
    port: 80
  };
  options.path = baseUrl + '&page=' + page;

  http.get(options,function(res){

    var body = "";
    res.setEncoding('utf8');
    res.on('data',function(chunk){
      body += chunk;
    });
    res.on('end', function(){
      var json = JSON.parse(body);
      notifyDone(page - 1,json);
    });
  }).on('error', function(e){
    console.log('Got error: ' + e.message);
  });
}
var downloadCnt = 0;
function startDownload(max_page){
  downloadCnt = max_page;
  for( var page = 0; page < max_page; page++){
    getUserTimeline(page+1);

  }
}

/* 一個のダウンロードが終わった. */
var results = [];
function notifyDone(page, json){
  results[page] = json;
  if(results.length == downloadCnt){
      notifyFinish();
  }
}

/* 全てのダウンロードが終わった */
function notifyFinish(){
  for(var i = 0, len = results.length; i < len; i++){
    var json = results[i];
    printJson(json);
  }
}

function printJson(json){
  for(var i = 0, len = json.length; i < len; i++){
    console.log(i+":"+json[i].created_at + " : " + json[i].text);
  }
}

startDownload(10);
